﻿Module Globals

    Public Sub bldUpdateSQL(ByVal fldName As String, ByRef sqlStr As String, ByVal valueToAdd As String, _
                      ByVal varType As Byte, ByVal cORe As Byte)

        Dim strWork As String
        '
        ' sqlStr = SQL string to build
        ' valueToAdd = Value to append
        ' varType = type of variable, 1 = string, 2 = number
        ' cORe = Flag that determines whether to continue or end the sql, 1 = continue, 2 = end.
        '
        sqlStr = sqlStr & fldName & " = "
        If varType = 1 Then
            '
            ' 1st must check to see if the string has imbedded single quote marks. If so, we will
            ' replace the single quote with 2 single quote marks.
            '
            strWork = valueToAdd
            strWork = Replace(strWork, "'", "''")
            sqlStr = sqlStr & "'" & strWork & "'"
        Else
            If varType = 2 Then
                If valueToAdd = "" Then
                    sqlStr = sqlStr & "0"
                Else
                    sqlStr = sqlStr & valueToAdd
                End If
            End If
        End If
        If cORe = 1 Then
            sqlStr = sqlStr & ", "
        Else
            If cORe = 2 Then
                sqlStr = sqlStr & " "
            End If
        End If

    End Sub

    Public Sub bldInsertSQL(ByRef sqlStr As String, ByVal valueToAdd As String, _
                          ByVal varType As Byte, ByVal cORe As Byte)

        Dim strWork As String
        '
        ' sqlStr = SQL string to add to
        ' valueToAdd = Value to append
        ' varType = type of variable, 1 = string, 2 = number
        ' cORe = Flag that determines whether to continue or end the sql, 1 = continue, 2 = end.
        '
        If varType = 1 Then
            '
            ' 1st must check to see if the string has imbedded single quote marks. If so, we will
            ' replace the single quote with 2 single quote marks.
            '
            strWork = valueToAdd
            strWork = Replace(strWork, "'", "''")
            sqlStr = sqlStr & "'" & strWork & "'"
        Else
            If varType = 2 Then
                If valueToAdd = "" Then
                    sqlStr = sqlStr & "0"
                Else
                    sqlStr = sqlStr & valueToAdd
                End If
            End If
        End If
        If cORe = 1 Then
            sqlStr = sqlStr & ", "
        Else
            If cORe = 2 Then
                sqlStr = sqlStr & ")"
            End If
        End If

    End Sub

End Module
