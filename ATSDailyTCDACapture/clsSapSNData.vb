﻿Public Class clsSapSNData

    Private prodOrdrVal As String
    Private materialVal As String
    Private modelVal As String
    Private phVal As String
    Private snExistsVal As Boolean

    Public Property prodOrdr() As String
        Get
            Return prodOrdrVal
        End Get
        Set(ByVal value As String)
            prodOrdrVal = value
        End Set
    End Property

    Public Property material() As String
        Get
            Return materialVal
        End Get
        Set(ByVal value As String)
            materialVal = value
        End Set
    End Property

    Public Property model() As String
        Get
            Return modelVal
        End Get
        Set(ByVal value As String)
            modelVal = value
        End Set
    End Property

    Public Property ph() As String
        Get
            Return phVal
        End Get
        Set(ByVal value As String)
            phVal = value
        End Set
    End Property

    Public Property snExists() As Boolean
        Get
            Return snExistsVal
        End Get
        Set(ByVal value As Boolean)
            snExistsVal = value
        End Set
    End Property

End Class
