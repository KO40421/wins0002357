﻿Public Class clsZATSRATE

    Private voltageVal As Integer
    Private phaseVal As Integer
    Private polesVal As Integer
    Private wiresVal As Integer
    Private enclosureVal As String
    Private ampsVal As Integer
    Private hertzVal As Integer
    Private switchTypeVal As String
    Private existsVal As Boolean

    Public Property voltage() As Integer
        Get
            Return voltageVal
        End Get
        Set(ByVal value As Integer)
            voltageVal = value
        End Set
    End Property

    Public Property phase() As Integer
        Get
            Return phaseVal
        End Get
        Set(ByVal value As Integer)
            phaseVal = value
        End Set
    End Property

    Public Property poles() As Integer
        Get
            Return polesVal
        End Get
        Set(ByVal value As Integer)
            polesVal = value
        End Set
    End Property

    Public Property wires() As Integer
        Get
            Return wiresVal
        End Get
        Set(ByVal value As Integer)
            wiresVal = value
        End Set
    End Property

    Public Property enclosure() As String
        Get
            Return enclosureVal
        End Get
        Set(ByVal value As String)
            enclosureVal = value
        End Set
    End Property

    Public Property amps() As Integer
        Get
            Return ampsVal
        End Get
        Set(ByVal value As Integer)
            ampsVal = value
        End Set
    End Property

    Public Property hertz() As Integer
        Get
            Return hertzVal
        End Get
        Set(ByVal value As Integer)
            hertzVal = value
        End Set
    End Property

    Public Property switchType() As String
        Get
            Return switchTypeVal
        End Get
        Set(ByVal value As String)
            switchTypeVal = value
        End Set
    End Property

    Public Property exists() As Boolean
        Get
            Return existsVal
        End Get
        Set(ByVal value As Boolean)
            existsVal = value
        End Set
    End Property

End Class
