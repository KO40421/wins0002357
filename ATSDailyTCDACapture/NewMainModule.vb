﻿Imports clsLogMessages
Imports SAP.Middleware.Connector
Imports clsSAPConnect
Imports System.Data.SqlClient
Imports clsGetSQLServerConnection
Module NewMainModule

    Dim sapConn As RfcDestination
    Dim processLogMsg As clsLogMessage
    Dim KPS_SQLConnection As SqlConnection
    Dim strSQL As String
    Dim taGEN_0091_ATSTEST As New KPS_SQLTableAdapters.GEN_0091_ATSTESTTableAdapter
    Dim GEN_0091_ATSTEST As New KPS_SQL.GEN_0091_ATSTESTDataTable
    Dim taMatlMast As New KPS_SQLTableAdapters.MatlMastTableAdapter
    Dim MatlMast As New KPS_SQL.MatlMastDataTable

    Sub Main()

        Dim iniXML As XDocument
        Dim sqlPath As String
        Dim logFileName As String
        Dim readOracle As New Odbc.OdbcConnection
        Dim updtOracle As New Odbc.OdbcConnection
        Dim uCmd As New Odbc.OdbcCommand
        Dim rCmd2 As New Odbc.OdbcCommand
        Dim rdr2 As Odbc.OdbcDataReader
        Dim runDate As Date
        Dim Yesterday As String
        Dim SAPData As clsSapSNData
        Dim ATSRateData As New clsZATSRATE
        Dim readRecs As Long
        Dim processedRecs As Long
        Dim snControl As String = ""
        Dim BoardSerial As String

        runDate = Now
        'runDate = #10/21/2020#
        Yesterday = runDate.Date.AddDays(-1)
        iniXML = XDocument.Load("\\Kowi30\Gendev\init.xml")
        logFileName = iniXML.Root.<logFilePath>.Value & "ATSDailyTCDACapture" & Format(runDate, "yyyyMMdd") & ".txt"
        processLogMsg = New clsLogMessage("ATSDailyTCDAResultsCapture", logFileName)
        '
        Try
            sqlPath = iniXML.Root.<sqlFolder>.Value
            'readOracle.ConnectionString = iniXML.Root.<PCORPConnectionString>.Value & ";pwd=kohler"    'Dsn=PKOHL;uid=ko40421
            readOracle.ConnectionString = "Dsn=PKOHL;uid=ko40421;pwd=kohler"
            readOracle.Open()
            'updtOracle.ConnectionString = iniXML.Root.<PCORPConnectionString>.Value & ";pwd=kohler"
            updtOracle.ConnectionString = "Dsn=PKOHL;uid=ko40421;pwd=kohler"
            updtOracle.Open()
            rCmd2.Connection = readOracle
            rCmd2.CommandType = CommandType.Text
            uCmd.Connection = updtOracle
            uCmd.CommandType = CommandType.Text
            '
            ' SPECIFY SAP CLIENT
            '
            Dim objDestConfig As IDestinationConfiguration = New ECCDestinationConfig
            RfcDestinationManager.RegisterDestinationConfiguration(objDestConfig)
            sapConn = RfcDestinationManager.GetDestination("PRD [Production]")
            '
            WriteLogRecord(clsLogMessage.informationalMessage, "SAP logon was successful")
            Console.WriteLine("SAP logon was successful")
            '
            Dim SQLConn As New clsSQLServerConnection
            'GenSQLServer = SQLConn.GetConnection("TEST", My.Application.Info.AssemblyName)
            KPS_SQLConnection = SQLConn.GetConnection("PROD", My.Application.Info.AssemblyName)
            If IsNothing(KPS_SQLConnection) Then Throw New ApplicationException("SQL Connection Issue")
            '
            taMatlMast.ClearBeforeFill = True
            taMatlMast.Connection = KPS_SQLConnection
            taGEN_0091_ATSTEST.ClearBeforeFill = True
            taGEN_0091_ATSTEST.Connection = KPS_SQLConnection
            '
            ' Read data from new ATS test table
            '
            taGEN_0091_ATSTEST.Fill(GEN_0091_ATSTEST, Yesterday)
            WriteLogRecord(clsLogMessage.startBatchProcess, "")
            If GEN_0091_ATSTEST.Rows.Count > 0 Then
                'If rdr1.HasRows Then
                Console.WriteLine()
                Console.WriteLine("Processing test records")
                WriteLogRecord(clsLogMessage.informationalMessage, "Processing test records")
                readRecs = 0
                processedRecs = 0
                For Each ATSrow As KPS_SQL.GEN_0091_ATSTESTRow In GEN_0091_ATSTEST.Rows
                    readRecs += 1
                    '
                    ' This check in conjunction with the snControl assignment after the record insert is to insure that the
                    ' latest passed test is the one that gets captured in oracle.
                    '
                    If snControl <> ATSrow.SERIAL_NUMBER Then
                        If Not ATSrow.SERIAL_NUMBER.StartsWith("LM") Then
                            SAPData = GetZratingData(ATSrow.SERIAL_NUMBER, ATSrow.PRODUCTION_ORDER)
                            If SAPData.snExists Then
                                ATSRateData = GetZATSRATEData(SAPData.model)
                                If Not ATSRateData.exists Then
                                    ATSRateData = GetZATSRATEData(SAPData.material)
                                    If Not ATSRateData.exists Then
                                        ATSRateData = GetZATSRATEData("_" & SAPData.model)
                                        If Not ATSRateData.exists Then
                                            ATSRateData = GetZATSRATEData("ES_" & SAPData.model)
                                        End If
                                    End If
                                End If
                                If ATSRateData.exists Then
                                    '
                                    ' Check to see if a serial master record exists for this serial number.
                                    '
                                    strSQL = "Select SERIAL_NUMBER, MODEL_DESIGNATION, PRODUCT_HIERARCHY, CREATE_DATE, PLANT FROM TBXR37_SERIALMASTER " &
                                                 "WHERE SERIAL_NUMBER = '" & ATSrow.SERIAL_NUMBER & "'"
                                    rCmd2.CommandText = strSQL
                                    rdr2 = rCmd2.ExecuteReader
                                    If rdr2.HasRows Then
                                        WriteLogRecord(clsLogMessage.informationalMessage, "Serial number " &
                                                       ATSrow.SERIAL_NUMBER & " is already in the Serial Master table.")
                                        rdr2.Close()
                                        '
                                        ' Check to see if there is an ATS config record
                                        '
                                        strSQL = "Select SERIAL_NUMBER FROM TBXR40_ATSCONFIG WHERE SERIAL_NUMBER = '" & ATSrow.SERIAL_NUMBER & "' AND " &
                                                 "MATERIAL = '" & SAPData.material & "'"
                                        rCmd2.CommandText = strSQL
                                        rdr2 = rCmd2.ExecuteReader
                                        If rdr2.HasRows Then
                                            '
                                            ' This will happen when an ATS is retested without a conversion order. We should update the test date and tester clock number
                                            '
                                            strSQL = "UPDATE GI06TCDA.TBXR40_ATSCONFIG SET TEST_TIMESTAMP = TO_TIMESTAMP('" & Format(ATSrow.TEST_TIMESTAMP, "dd-MMM-yyyy HH:mm:ss.ff") &
                                                              "', 'DD-Mon-RR HH24:MI:SS.FF'), TESTER_CLOCK_NO  = '" & ATSrow.TESTER_CLOCK_NO &
                                                              "' WHERE SERIAL_NUMBER = '" & ATSrow.SERIAL_NUMBER & "' AND MATERIAL = '" & SAPData.material & "'"
                                            uCmd.CommandText = strSQL
                                            Try
                                                uCmd.ExecuteNonQuery()
                                                processedRecs += 1
                                                snControl = ATSrow.SERIAL_NUMBER
                                            Catch ex As Exception
                                                WriteLogRecord(clsLogMessage.recoverableErrorMessage, "An error occured while updating ATS " &
                                                               "Config data. Save operation failed! Exception messsage = " & ex.Message)
                                            End Try
                                            WriteLogRecord(clsLogMessage.informationalMessage, "Serial number " &
                                                       ATSrow.SERIAL_NUMBER & " is already in the ATS Config table. Test date and tester has been updated.")
                                        Else
                                            strSQL = "Insert Into TBXR40_ATSCONFIG (SERIAL_NUMBER, MATERIAL, TEST_TIMESTAMP, PRODUCTION_ORDER, " &
                                                     "VOLTAGE, PHASE, POLES, WIRES, ENCLOSURE, AMPS, HERTZ, SWITCH_TYPE, CONTACTOR_PART, " &
                                                     "CONTACTOR_SER, CNTL_BOARD_SER, TESTER_CLOCK_NO, CODE_VERSION, PLANT) VALUES ("
                                            bldInsertSQL(strSQL, ATSrow.SERIAL_NUMBER, 1, 1)
                                            bldInsertSQL(strSQL, SAPData.material, 1, 1)
                                            strSQL = strSQL & "TO_TIMESTAMP('" & Format(ATSrow.TEST_TIMESTAMP, "dd-MMM-yyyy HH:mm:ss.ff") &
                                                              "', 'DD-Mon-RR HH24:MI:SS.FF'), "
                                            bldInsertSQL(strSQL, SAPData.prodOrdr, 1, 1)
                                            bldInsertSQL(strSQL, ATSRateData.voltage, 2, 1)
                                            bldInsertSQL(strSQL, ATSRateData.phase, 2, 1)
                                            bldInsertSQL(strSQL, ATSRateData.poles, 2, 1)
                                            bldInsertSQL(strSQL, ATSRateData.wires, 2, 1)
                                            bldInsertSQL(strSQL, ATSRateData.enclosure, 1, 1)
                                            bldInsertSQL(strSQL, ATSRateData.amps, 2, 1)
                                            bldInsertSQL(strSQL, ATSRateData.hertz, 2, 1)
                                            bldInsertSQL(strSQL, ATSRateData.switchType, 1, 1)
                                            bldInsertSQL(strSQL, ATSrow.CONTACTOR_PART, 1, 1)
                                            bldInsertSQL(strSQL, ATSrow.CONTACTOR_SER, 1, 1)
                                            BoardSerial = ATSrow.CNTL_BOARD_SER.Trim
                                            If BoardSerial.Length > 15 Then BoardSerial = BoardSerial.Substring(0, 15)
                                            bldInsertSQL(strSQL, BoardSerial, 1, 1)
                                            bldInsertSQL(strSQL, ATSrow.TESTER_CLOCK_NO, 1, 1)
                                            bldInsertSQL(strSQL, ATSrow.CODE_VERSION, 1, 1)
                                            bldInsertSQL(strSQL, "GM", 1, 2)
                                            uCmd.CommandText = strSQL
                                            Try
                                                uCmd.ExecuteNonQuery()
                                                processedRecs += 1
                                                snControl = ATSrow.SERIAL_NUMBER
                                            Catch ex As Exception
                                                WriteLogRecord(clsLogMessage.recoverableErrorMessage, "An error occured while saving ATS " &
                                                               "Config data. Save operation failed! Exception messsage = " & ex.Message)
                                            End Try
                                        End If
                                    Else
                                        strSQL = "INSERT INTO TBXR37_SERIALMASTER (SERIAL_NUMBER, MODEL_DESIGNATION, PRODUCT_HIERARCHY, PLANT) VALUES ("
                                        bldInsertSQL(strSQL, ATSrow.SERIAL_NUMBER, 1, 1)
                                        bldInsertSQL(strSQL, SAPData.model, 1, 1)
                                        bldInsertSQL(strSQL, SAPData.ph, 1, 1)
                                        bldInsertSQL(strSQL, "GM", 1, 2)
                                        uCmd.CommandText = strSQL
                                        Try
                                            uCmd.ExecuteNonQuery()
                                            strSQL = "Insert Into TBXR40_ATSCONFIG (SERIAL_NUMBER, MATERIAL, TEST_TIMESTAMP, PRODUCTION_ORDER, " &
                                                     "VOLTAGE, PHASE, POLES, WIRES, ENCLOSURE, AMPS, HERTZ, SWITCH_TYPE, CONTACTOR_PART, " &
                                                     "CONTACTOR_SER, CNTL_BOARD_SER, TESTER_CLOCK_NO, CODE_VERSION, PLANT) VALUES ("
                                            bldInsertSQL(strSQL, ATSrow.SERIAL_NUMBER, 1, 1)
                                            bldInsertSQL(strSQL, SAPData.material, 1, 1)
                                            strSQL = strSQL & "TO_TIMESTAMP('" & Format(ATSrow.TEST_TIMESTAMP, "dd-MMM-yyyy HH:mm:ss.ff") &
                                                              "', 'DD-Mon-RR HH24:MI:SS.FF'), "
                                            bldInsertSQL(strSQL, SAPData.prodOrdr, 1, 1)
                                            bldInsertSQL(strSQL, ATSRateData.voltage, 2, 1)
                                            bldInsertSQL(strSQL, ATSRateData.phase, 2, 1)
                                            bldInsertSQL(strSQL, ATSRateData.poles, 2, 1)
                                            bldInsertSQL(strSQL, ATSRateData.wires, 2, 1)
                                            bldInsertSQL(strSQL, ATSRateData.enclosure, 1, 1)
                                            bldInsertSQL(strSQL, ATSRateData.amps, 2, 1)
                                            bldInsertSQL(strSQL, ATSRateData.hertz, 2, 1)
                                            bldInsertSQL(strSQL, ATSRateData.switchType, 1, 1)
                                            bldInsertSQL(strSQL, ATSrow.CONTACTOR_PART, 1, 1)
                                            bldInsertSQL(strSQL, ATSrow.CONTACTOR_SER, 1, 1)
                                            BoardSerial = ATSrow.CNTL_BOARD_SER.Trim
                                            If BoardSerial.Length > 15 Then BoardSerial = BoardSerial.Substring(0, 15)
                                            bldInsertSQL(strSQL, BoardSerial, 1, 1)
                                            bldInsertSQL(strSQL, ATSrow.TESTER_CLOCK_NO, 1, 1)
                                            bldInsertSQL(strSQL, ATSrow.CODE_VERSION, 1, 1)
                                            bldInsertSQL(strSQL, "GM", 1, 2)
                                            uCmd.CommandText = strSQL
                                            Try
                                                uCmd.ExecuteNonQuery()
                                                processedRecs += 1
                                                snControl = ATSrow.SERIAL_NUMBER
                                            Catch ex As Exception
                                                WriteLogRecord(clsLogMessage.recoverableErrorMessage, "An error occured while saving ATS " &
                                                               "Config data. Save operation failed! Exception messsage = " & ex.Message)
                                            End Try
                                        Catch ex As Exception
                                            WriteLogRecord(clsLogMessage.recoverableErrorMessage, "An error occured while saving ATS " &
                                                           "Config data. Save operation failed! Exception messsage = " & ex.Message)
                                        End Try
                                    End If
                                    rdr2.Close()
                                Else
                                    '
                                    ' Don't have rating info for this switch. Must log an error msg.
                                    '
                                    WriteLogRecord(clsLogMessage.recoverableErrorMessage, "Did not find model " & SAPData.model &
                                                   " or material " & SAPData.material & " in ZATSRATE")
                                End If
                            Else
                                WriteLogRecord(clsLogMessage.recoverableErrorMessage, "Did not find serial number " &
                                               ATSrow.SERIAL_NUMBER & " in ZRATING")
                            End If
                        End If
                    End If
                    Console.Write(vbCr & readRecs & " records read " & processedRecs & " records processed")
                Next
                WriteLogRecord(clsLogMessage.informationalMessage, readRecs & " records read " & processedRecs & " records processed")
            End If
            readOracle.Close()
            updtOracle.Close()
            KPS_SQLConnection.Close()
            WriteLogRecord(clsLogMessage.endBatchProcess, "")
            processLogMsg.Close()
        Catch ex As Exception
            WriteLogRecord(clsLogMessage.terminalErrorMessage, "Function GetZratingData - " & ex.Source & " - " & ex.Message)
            processLogMsg.Close()
            Environment.Exit(1)  'Exit the app returning the "unrecoverable error" code, code 1.
        End Try

    End Sub

    Private Function BuildLogMsg(ByVal msgText As String, ByVal sn As String, ByVal ts As Date) As String

        Dim rVal As String = ""

        Try
            rVal = msgText & vbTab & sn & vbTab & ts
        Catch ex As Exception
            WriteLogRecord(clsLogMessage.terminalErrorMessage, "Function BuildErrMsg - " & ex.Source & " - " & ex.Message)
        End Try
        Return rVal

    End Function

    Private Sub WriteLogRecord(ByVal mType As Integer, ByVal lMsg As String)

        Try
            processLogMsg.Type = mType
            processLogMsg.Text = lMsg
            processLogMsg.WriteLogFileEntry()
        Catch ex As Exception
            Environment.Exit(1)  'Exit the app returning the "unrecoverable error" code, code 1.
        End Try

    End Sub

    Private Function GetZratingData(ByVal SN As String, Optional ByVal PrdOrd As String = "") As clsSapSNData

        Dim readTable_Zrating As IRfcFunction = sapConn.Repository.CreateFunction("Z_AW_RFC_READ_TABLE")
        Dim Zrating_Fields As IRfcTable
        Dim Zrating_Options As IRfcTable
        Dim Zrating_Data As IRfcTable
        Dim idx As Integer
        Dim sapPrdOrd As String = Format(Val(PrdOrd), "000000000000")
        Dim rVal As New clsSapSNData
        Dim offset As Integer
        Dim length As Integer
        Dim NoOfItms As Integer
        Dim materialNumber As String
        Dim prodOrdrNumber As String
        Dim model_desig As String
        Dim CreateDate As String
        Dim maxCreateDate As String
        Dim maxCreateDateItem As Integer
        Dim mmRow As KPS_SQL.MatlMastRow

        'Try

        rVal.prodOrdr = ""
            rVal.material = ""
            rVal.ph = ""
            rVal.model = ""

            Zrating_Fields = readTable_Zrating.GetTable("FIELDS")
            Zrating_Options = readTable_Zrating.GetTable("OPTIONS")
            Zrating_Data = readTable_Zrating.GetTable("DATA")
            '
            ' Specify table to read
            '
            readTable_Zrating.SetValue("QUERY_TABLE", "ZRATING")
            '
            ' Specify field names to extract
            '
            Zrating_Fields.Clear()
            Zrating_Fields.Append()
            idx = Zrating_Fields.RowCount - 1
            Zrating_Fields(idx).SetValue("FIELDNAME", "AUFNR")     'Production Order Number
            Zrating_Fields.Append()
            idx = Zrating_Fields.RowCount - 1
            Zrating_Fields(idx).SetValue("FIELDNAME", "PLNBEZ")    'Material Number
            Zrating_Fields.Append()
            idx = Zrating_Fields.RowCount - 1
            Zrating_Fields(idx).SetValue("FIELDNAME", "ATS_MODEL") 'Model Designation 
            Zrating_Fields.Append()
            idx = Zrating_Fields.RowCount - 1
            Zrating_Fields(idx).SetValue("FIELDNAME", "ERDAT")     'Create date
            '
            ' Specify Selection Criteria
            '
            Zrating_Options.Clear()
            Zrating_Options.Append()
            idx = Zrating_Options.RowCount - 1
            '
            ' Production query for selecting materials to add/update
            '
            If PrdOrd = "" Then
                Zrating_Options(idx).SetValue("TEXT", "SERIAL_NUM EQ '" & SN & "'")
            Else
                Zrating_Options(idx).SetValue("TEXT", "SERIAL_NUM EQ '" & SN & "' AND AUFNR EQ '" & sapPrdOrd & "'")
            End If
            Zrating_Data.Clear()
            '
            ' Execute query
            '
            readTable_Zrating.Invoke(sapConn)
            If Zrating_Data.RowCount > 0 Then
                WriteLogRecord(clsLogMessage.informationalMessage, "Found serial number " & SN & " in ZRATING")
                NoOfItms = Zrating_Data.RowCount
                maxCreateDate = ""
                maxCreateDateItem = 0
                For X = 0 To NoOfItms - 1
                    offset = Zrating_Fields(3).GetInt("OFFSET") + 1
                    length = Zrating_Fields(3).GetInt("LENGTH")
                    CreateDate = Trim(Mid(Zrating_Data(X).GetString("WA"), offset, length))
                    If CreateDate > maxCreateDate Then
                        maxCreateDate = CreateDate
                        maxCreateDateItem = X
                    End If
                Next
                offset = Zrating_Fields(0).GetInt("OFFSET") + 1
                length = Zrating_Fields(0).GetInt("LENGTH")
                prodOrdrNumber = Trim(Mid(Zrating_Data(maxCreateDateItem).GetString("WA"), offset, length))
                offset = Zrating_Fields(1).GetInt("OFFSET") + 1
                length = Zrating_Fields(1).GetInt("LENGTH")
                materialNumber = Trim(Mid(Zrating_Data(maxCreateDateItem).GetString("WA"), offset, length))
                offset = Zrating_Fields(2).GetInt("OFFSET") + 1
                length = Zrating_Fields(2).GetInt("LENGTH")
                model_desig = Trim(Mid(Zrating_Data(maxCreateDateItem).GetString("WA"), offset, length))
                rVal.prodOrdr = prodOrdrNumber
                rVal.material = materialNumber
                rVal.model = model_desig
                taMatlMast.Fill(MatlMast, materialNumber)
                If MatlMast.Rows.Count > 0 Then
                    mmRow = MatlMast.Rows(0)
                    rVal.ph = mmRow.ProductHierarchyDC
                End If
                rVal.snExists = True
            Else
                WriteLogRecord(clsLogMessage.recoverableErrorMessage, "Did not find serial number " & SN & " in ZRATING")
                rVal.snExists = False
            End If
        'Catch ex As Exception
        '    WriteLogRecord(clsLogMessage.terminalErrorMessage, "Function GetZratingData - " & ex.Source & " - " & ex.Message)
        '    processLogMsg.Close()
        '    Environment.Exit(1)  'Exit the app returning the "unrecoverable error" code, code 1.
        'End Try
        readTable_Zrating = Nothing
        Return rVal

    End Function

    Private Function GetZATSRATEData(ByVal atsKey As String) As clsZATSRATE

        Dim readTable_ZATSRATE As IRfcFunction = sapConn.Repository.CreateFunction("Z_AW_RFC_READ_TABLE")
        Dim ZATSRATE_Fields As IRfcTable
        Dim ZATSRATE_Options As IRfcTable
        Dim ZATSRATE_Data As IRfcTable
        Dim idx As Integer
        Dim rVal As New clsZATSRATE
        Dim offset As Integer
        Dim length As Integer

        'Try
        ZATSRATE_Fields = readTable_ZATSRATE.GetTable("FIELDS")
            ZATSRATE_Options = readTable_ZATSRATE.GetTable("OPTIONS")
            ZATSRATE_Data = readTable_ZATSRATE.GetTable("DATA")
            '
            ' Specify table to read
            '
            readTable_ZATSRATE.SetValue("QUERY_TABLE", "ZATSRATE")
            '
            ' Specify field names to extract
            '
            ZATSRATE_Fields.Clear()
            ZATSRATE_Fields.Append()
            idx = ZATSRATE_Fields.RowCount - 1
            ZATSRATE_Fields(idx).SetValue("FIELDNAME", "VOLTAGE")
            ZATSRATE_Fields.Append()
            idx = ZATSRATE_Fields.RowCount - 1
            ZATSRATE_Fields(idx).SetValue("FIELDNAME", "PHASE")
            ZATSRATE_Fields.Append()
            idx = ZATSRATE_Fields.RowCount - 1
            ZATSRATE_Fields(idx).SetValue("FIELDNAME", "POLES")
            ZATSRATE_Fields.Append()
            idx = ZATSRATE_Fields.RowCount - 1
            ZATSRATE_Fields(idx).SetValue("FIELDNAME", "WIRES")
            ZATSRATE_Fields.Append()
            idx = ZATSRATE_Fields.RowCount - 1
            ZATSRATE_Fields(idx).SetValue("FIELDNAME", "ATS_ENCL")
            ZATSRATE_Fields.Append()
            idx = ZATSRATE_Fields.RowCount - 1
            ZATSRATE_Fields(idx).SetValue("FIELDNAME", "ATS_AMPS")
            ZATSRATE_Fields.Append()
            idx = ZATSRATE_Fields.RowCount - 1
            ZATSRATE_Fields(idx).SetValue("FIELDNAME", "ATS_HERTS")
            ZATSRATE_Fields.Append()
            idx = ZATSRATE_Fields.RowCount - 1
            ZATSRATE_Fields(idx).SetValue("FIELDNAME", "SWITCH")
            '
            ' Specify Selection Criteria
            '
            ZATSRATE_Options.Clear()
            ZATSRATE_Options.Append()
            idx = ZATSRATE_Options.RowCount - 1
            ZATSRATE_Options(idx).SetValue("TEXT", "ATS_MODEL EQ '" & atsKey & "'")
            '
            ZATSRATE_Data.Clear()
            '
            ' Execute query
            '
            readTable_ZATSRATE.Invoke(sapConn)
            If ZATSRATE_Data.RowCount > 0 Then
                rVal.exists = True
                offset = ZATSRATE_Fields(0).GetInt("OFFSET") + 1
                length = ZATSRATE_Fields(0).GetInt("LENGTH")
                rVal.voltage = Trim(Mid(ZATSRATE_Data(0).GetString("WA"), offset, length))
                offset = ZATSRATE_Fields(1).GetInt("OFFSET") + 1
                length = ZATSRATE_Fields(1).GetInt("LENGTH")
                rVal.phase = Trim(Mid(ZATSRATE_Data(0).GetString("WA"), offset, length))
                offset = ZATSRATE_Fields(2).GetInt("OFFSET") + 1
                length = ZATSRATE_Fields(2).GetInt("LENGTH")
                rVal.poles = Trim(Mid(ZATSRATE_Data(0).GetString("WA"), offset, length))
                offset = ZATSRATE_Fields(3).GetInt("OFFSET") + 1
                length = ZATSRATE_Fields(3).GetInt("LENGTH")
                rVal.wires = Trim(Mid(ZATSRATE_Data(0).GetString("WA"), offset, length))
                offset = ZATSRATE_Fields(4).GetInt("OFFSET") + 1
                length = ZATSRATE_Fields(4).GetInt("LENGTH")
                rVal.enclosure = Trim(Mid(ZATSRATE_Data(0).GetString("WA"), offset, length))
                offset = ZATSRATE_Fields(5).GetInt("OFFSET") + 1
                length = ZATSRATE_Fields(5).GetInt("LENGTH")
                rVal.amps = Trim(Mid(ZATSRATE_Data(0).GetString("WA"), offset, length))
                offset = ZATSRATE_Fields(6).GetInt("OFFSET") + 1
                length = ZATSRATE_Fields(6).GetInt("LENGTH")
                rVal.hertz = Trim(Mid(ZATSRATE_Data(0).GetString("WA"), offset, length))
                offset = ZATSRATE_Fields(7).GetInt("OFFSET") + 1
                length = ZATSRATE_Fields(7).GetInt("LENGTH")
                rVal.switchType = Trim(Mid(ZATSRATE_Data(0).GetString("WA"), offset, length))
            Else
                rVal.exists = False
            End If
        'Catch ex As Exception
        '    WriteLogRecord(clsLogMessage.terminalErrorMessage, "Function GetZratingData - " & ex.Source & " - " & ex.Message)
        '    processLogMsg.Close()
        '    Environment.Exit(1)  'Exit the app returning the "unrecoverable error" code, code 1.
        'End Try
        readTable_ZATSRATE = Nothing
        Return rVal

    End Function

End Module
